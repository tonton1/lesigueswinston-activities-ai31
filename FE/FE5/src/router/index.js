import Vue from 'vue'
import VueRouter from 'vue-router'
import uploadmusic from '../views/uploadmusic'

Vue.use(VueRouter)

const routes = [
  {
    path: '',
    component: uploadmusic,
    children: [
      {
        path: '',
        component: () => import(/* webpackChunkName: "Overview" */ '../views/uploadmusic.vue')
      },
      {
        path: 'allsong',
        component: () => import(/* webpackChunkName: "Messages" */ '../views/allsong.vue')
      },
      {
        path: 'createplaylist',
        component: () => import(/* webpackChunkName: "Profile" */ '../views/createplaylist.vue')
      },
      
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
